# Recalbox input lag tools

Measure GPIO, USB or BT gamepad input lag on Raspberry Pi.

## How it works:

The program measures the time between the moment the joystick button was pressed and the moment the system received the
information.

It has two modes:

- **auto**: use a gpio to control a gamepad button. Can measure several samples really quickly.
- **manual**: check the value of the button on a gpio. The user must press the button manually.


## Latency

Latency is the time it takes between the moment you press a button and the moment the image changes on the screen.

The latency of a game is induced by several elements, which we will simplify as follows:
* **Input Lag**: this is the time it takes for a controller to send information to the console/system.
* **Process Lag**: this is the time it takes for the system, after receiving the information from the joystick, to calculate and start sending the video signal.
* **Display Lag**: the time it takes for the screen to display (or start to display) the image.

```mermaid
gantt
    title The Latency
    dateFormat SSS
    axisFormat %L
    section Timeline
    Button Pressed (T0) :milestone, 0,0
    Input Lag :a1, 0, 12ms
    Event received :milestone, 0
    Process Lag :done, a2,after a1, 30ms
    Sending frame to video device (T1) :milestone,
    Display Lag :a3,after a2, 20ms
    Image displayed (T2) :milestone,
```

With the input lag tool, you will be able to measure the **Input Lag** of your controller.


## Installation

You need to install pip and gpiozero to run the program:

```bash
python -m ensurepip --upgrade
python -m pip install gpiozero
```

## Usage

Detect your controller id:

```bash
for gamepad in /sys/class/input/js*; do echo "$gamepad -> $(cat $gamepad/device/name)"; done
```

```bash
/sys/class/input/js0 -> Generic X-Box pad
```

**0** will be the index of your controller.

Then start the detection of the **buttonByte**:

```bash
python lag-v1.0.py --gpio=0 --gamepad=0 --detect
```

```bash
Press the button you want to use (1 times).
Press the button you want to use (0 times).
Found button on byte: 4
You can start the command with: 
python3 lag-v1.0.py --gamepad=0 --buttonByte=4
```

Solder your GPIO (4 by default) on the 3.3V pin of the controller button.

Please be sure the controller logic level is 3.3V and not 5V.

Then start the measure (here with for gpio 0):

```bash
python lag-v1.0.py --gamepad=0 --buttonByte=4 --gpio=0
```

```
....
Average: 0.5425307828282828
Lag in ms:  0.537289
Lag in µs:       537
Average: 0.5425044422110553
Lag in ms:   0.52464
Lag in µs:       524
Average: 0.54241512
Results for gamepad 0 on gpio 0, mode auto 
Average input lag: 0.54241512ms (542.41512µs)
Closing the device
```

### Measures:

| Controller                 | Protocol | Board | Average Lag  in Micro Seconds | Average Lag  in Millis Seconds | Average Lag  In Frames |
|----------------------------|----------|-------|-------------------------------|--------------------------------|------------------------|
| Recalbox RGB JAMMA         | I2C      | RPI4  | 378,00                        | 0,378                          | 2,27%                  |
| Raspberry Pi Pico + GP2040 | USB      | RPI4  | 800,00                        | 0,8                            | 4,80%                  |
| Brooke Ultimate            | USB      | RPI4  | 901,00                        | 0,901                          | 5,41%                  |
| Retroflag Megadrive USB    | USB      | RPI4  | 3 018,00                      | 3,018                          | 18,11%                 |
| XinMotek 2Players          | USB      | RPI4  | 4 088,00                      | 4,088                          | 24,53%                 |
| RGB-PI JAMMA               | I2C      | RPI4  | 4 271,00                      | 4,271                          | 25,63%                 |
| Brooks Zero Pi             | USB      | RPI4  | 6 392,00                      | 6,392                          | 38,35%                 |
| RPi2Jamma - AJE            |          | RPI3  | 9 397,00                      | 9,397                          | 56,38%                 |
| EG Starts (DragonRise)     | USB      | RPI4  | 13 737,00                     | 13,737                         | 82,42%                 |


### Updates

- Updated Recalbox RGB JAMMA data after last firmware update.
- Added new devices

### Coming soon

- Mister FPGA measures (now you have to do it :D )


### Contribute

- Please open merge requests with your input measures or script improvements. 

### Credits:

Author: **@digitalLumberjack** from [recalbox](https://www.recalbox.com)
