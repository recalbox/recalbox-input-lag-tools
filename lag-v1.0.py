import sys

import time
import getopt
from gpiozero import DigitalOutputDevice, Button

command = "measure"
manual = False
samples = 200
pullUp = True
debug = False
detect = False
gpioNum = 4
gamepad = 0
buttonByte = 4

try:
    opts, args = getopt.getopt(sys.argv[1:], "lmdpg",
                               ["manual", "debug", "pulldown", "gpio=", "detect", "gamepad=", "buttonByte=",
                                "samples="])
except getopt.GetoptError:
    print(opts)

for opt, arg in opts:
    if opt in ("-l", "--list"):
        command = "list"
    elif opt in ("-m", "--manual"):
        print("manual mode")
        manual = True
    elif opt in ("-p", "--pulldown"):
        print("pull down")
        pullUp = False
    elif opt in ("-d", "--debug"):
        print("debug")
        debug = True
    elif opt in ("-g", "--gpio"):
        gpioNum = arg
    elif opt == "--detect":
        command = "detect"
    elif opt == "--gamepad":
        gamepad = arg
    elif opt == "--byte":
        buttonByte = arg
    elif opt == "--samples":
        samples = int(arg)


def debugLog(string: str):
    if debug:
        print(string)


try:

    time.sleep(0.05)
    if manual:
        print("Setting manual mode on gpio {}".format(gpioNum))
        button = Button(gpioNum, pull_up=pullUp)
    else:
        print("Setting auto mode on gpio {}".format(gpioNum))
        gpio = DigitalOutputDevice(gpioNum, active_high=True)
        gpio.on()

    from threading import Thread

    start = time.time_ns()
    total = 0
    loops = 0
    reset = True
    started = False
    running = True

    def gpioButtonPress():
        global start, reset, started

        time.sleep(4)
        started = True
        while running:
            if reset:
                reset = False
                gpio.on()
                time.sleep(0.1)
                debugLog("gpio off")
                gpio.off()
                start = time.time_ns()
            else:
                debugLog("waiting")
                time.sleep(0.1)


    def gpioButtonEvent():
        global start, reset
        debugLog("manual button pressed")
        reset = False
        start = time.time_ns()


    if command == "measure":
        if manual:
            button.when_pressed = gpioButtonEvent
            started = True
        else:
            t = Thread(target=gpioButtonPress)
            t.start()
            debugLog("Thread started")

    chunk_size = 8
    candidats = [0, 0, 0, 0, 0, 0, 0, 0]
    old = None
    with open("/dev/input/js{}".format(gamepad), "rb") as in_file:
        debugLog("/dev/input/js{} opened".format(gamepad))
        while True:
            d = in_file.read(chunk_size)
            debugLog(d.hex())
            if command == "detect":
                if old is not None and old != d:
                    for byte in range(0, 8):
                        if candidats[byte] == 10:
                            print(
                                "Found button on byte: {}\nYou can start the command with: \npython3 lag-v1.0.py --gamepad={} --buttonByte={}".format(
                                    byte, gamepad, byte))
                            exit(0)
                        if old[byte] == 1 and d[byte] == 0:
                            candidats[byte] += 1
                print("Press the button you want to use ({} times).".format(10 - max(candidats)))
                old = d
            else:
                if d[buttonByte] & 1 and started:
                    timediff = time.time_ns() - start
                    loops += 1
                    total += timediff
                    reset = True
                    print("Lag in ms: {:>9}".format(timediff / 1000000))
                    print("Lag in µs: {:>9}".format(int((timediff) / 1000)))
                    print("Average: {}".format((total / loops) / 1000000))
                    if loops >= samples:
                        print("Results for gamepad {} on gpio {}, mode {}".format(gamepad, gpioNum,
                                                                                "manual" if manual else "auto"))
                        print("Average input lag: {:>9}ms ({:>9}µs)".format((total / loops) / 1000000,
                                                                            (total / loops) / 1000))
                        break

    print("Closing the device")
    running = False

except IOError as ex:
    print(ex)
